#!/bin/bash
# A part of xadf dotfiles repository
# Copyright 2023, Hendrik Lie, licensed under GPLv3
# See branch master for LICENSE

##################################################################################
## Functions
##################################################################################

# A function to parse git branch, taken from: https://stackoverflow.com/a/35218509
# Git branch in prompt.
__git_branch() {
    git -c color.ui=always branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

##################################################################################
## Variables
##################################################################################

# Color config files
cfgc=( "$xadfshare/xadf_prompt_colors.txt" "$xadfconfig/xadf_prompt_colors.txt" )

# Prompt config files
cfgps=( "$xadfconfig/xadf_prompts.txt" "$xadfshare/xadf_prompts.txt" )

# Now we want to filter the kinds of operating system we're on
test -f /etc/os-release && xadf_distro_name=$(grep ^NAME /etc/os-release|sed 's_^NAME=__;s_\"__g')

##################################################################################
## Functions
##################################################################################

# Parses file $1 and outputs the filtered contents in bash variable assignment notation
colorcat(){
local file
file="$1"
[[ "$file" == "" ]] && file='file.txt'
# Protect with test clause so would not perform cat and grep for nonexisting files
test -f "$file" && cat <(grep -E '^[a-zA-Z_].+=' "$file"|grep -v '[^a-zA-Z_0-9].*=') || true
}

# Parses file $1 and outputs the filtered contents in bash associative array variable
# assignment notation. Target array is $2, or $target if not specified. Note that the
# variable still needs to be declared as an associative array (eg. declare -A target)
promptcat(){
file="$1"
[[ "$file" == "" ]] && file='file.txt'

array="$2"
[[ "$array" == "" ]] && array='target'

unset temparray "$array"
declare -A "$array"

# Parse file into an array
while read -r line ; do export temparray+=("$line") ; done < "$file"

# Parse array into associative array
for i in ${!temparray[@]} ; do
  echo "${temparray[$i]}"|grep -E '^[a-zA-Z_].+='|grep -v '[^a-zA-Z_0-9].*='|sed -E "s|^([a-zA-Z_0-9]+)=(.*)$|${array}+=(\1 \"\2\")|g"
done
}

# Modifies PS1 as needed.
psmod(){
local style file
style="$1"

# Traverses array $cfgps[@] and loads only the one available
# first, whose path is added to local var $file
if test -f "${cfgps[0]}" ; then file="${cfgps[0]}"
elif test -f "${cfgps[1]}" ; then file="${cfgps[1]}"
else true ; fi

# Creates associative array xadf_prompt
declare -A xadf_prompt
. <(promptcat "$file" xadf_prompt)

# Main code block
case $style in
  --help|-h)
    printf "Commands:\n\n"
    printf "    psmod <style>\t\tset style to <style>\n"
    printf "    psmod --list/-l\t\tlist styles\n"
    printf "    psmod --help/-h\t\tshow this help text\n\n"
    ;;
  --list|-l)
    printf "Existing prompt styles:\n\n"
    for i in ${!xadf_prompt[@]}
    do
      printf " --> %s\n" "$i"
    done | sort
    printf "\nTo select a prompt style:\n"
    printf "  psmod <style>\n\n"
    ;;
  "")
    printf >&2 "No arguments! See 'psmod --help'\n\n"
    ;;
  *)
    if [[ -n "${xadf_prompt[$style]}" ]]
    then
      # Had to add a trailing space for most of the styles to work
      unset PS1 && export PS1="${xadf_prompt[$style]}"
    else
      printf >&2 "Specified style does not exist! See 'psmod --list' for list of styles\n\n"
    fi
    ;;
esac
}

# Probably obsolete, we will have to check them later
promptstyler(){
test -n "$1" && verbosity="$1"

if [ -z "$xadf_distro_name" ] # most probably running termux
then
    # Testing if it is termux
    if command -v termux-setup-storage > /dev/null
    then
        test "$verbosity" = "v" && echo "In Termux"
        unset PS1 user_color
        export PS1="${xadf_prompt[termux]}"
    fi
elif [[ "$xadf_distro_name" == "Arch Linux" ]]
then # Most likely in Arch
    test "$verbosity" = "v" && echo "In Arch"
    unset PS1 user_color
    export user_color=$cyan
    export PS1="${xadf_prompt[arch]}"
elif [[ "$xadf_distro_name" == "Fedora Linux" ]]
then # Most likely in Fedora
    test "$verbosity" = "v" && echo "In Fedora"
    unset PS1 user_color
    export user_color=$magenta
    export PS1="${xadf_prompt[fedora]}"
elif [[ "$xadf_distro_name" == "Ubuntu" ]]
then # Most likely in Ubuntu
    test "$verbosity" = "v" && echo "In Ubuntu"
    unset PS1
    user_color=$spacedust
    PS1="${xadf_prompt[ubuntu_default]}"
else # We are not sure which linux are we running on
    test "$verbosity" = "v" && echo "Not sure where"
    unset PS1 user_color
    export user_color=$spacedust
    export PS1="${xadf_prompt[arch_plain]}"
fi
}

##################################################################################
## Invocations
##################################################################################

# Custom configs at $cfgc[1] will override those already set by $cfgc[0]
. <(colorcat "${cfgc[0]}")
. <(colorcat "${cfgc[1]}")

