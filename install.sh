#!/bin/bash
# xadf installer script

installdir="$HOME/.local/bin"
xadfsource="https://gitlab.com/xadfsh/xadf/-/raw/master/.local/bin/xadf"
targetexec="$installdir/xadf"
timehash="$(date +%s|md5sum)"
tmpdir="$HOME/.tmp-xadf-${timehash:0:8}"

# Make directory, if it isn't present already
test ! -d "$installdir" && mkdir -p "$installdir"

# Download the executable into your local bin directory
if command -v wget 2>&1 > /dev/null
then
    wget -O "$targetexec" "$xadfsource"
elif command -v curl 2>&1 > /dev/null
then
    curl -so "$targetexec" "$xadfsource"
else
    echo "Error! Neither wget or curl is present in the system! Install either one of them!"
    exit 0
fi

# Make it executable
chmod +x "$targetexec"

# Install xadf minimally
"$targetexec" --minimal-install --xadf-seat "$tmpdir"

# Removes temporary directory
rm -rf "$tmpdir"
