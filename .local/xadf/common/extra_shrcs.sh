#!/bin/bash
###############################################################################
#
#   xadf - extra_shrcs.sh
#   Copyright (C) 2023  Hendrik Lie
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   Author: Hendrik Lie
#   Email : <hendriklie72@gmail.com>
#
###############################################################################
#
# SUMMARY
# An xadfrc module that reads alternative bashrc or zshrc file. When the module
# is used, it will first determine the kind of shell it is run from. Then it
# checks whether custom {ba,z}shrc on $xadfconfig is present, and sources it.
# If it fails, it would load from $xadfmods instead.

if test -n "$BASH_VERSION" ; then
    # Sources $xadfconfig/bashrc if exist, otherwise sources $xadfmods/bashrc
    test -f "$xadfconfig/bashrc" && source "$xadfconfig/bashrc" || source "$xadfmods/bashrc"
fi
if test -n "$ZSH_VERSION" ; then
    # Sources $xadfconfig/zshrc if exist, otherwise sources $xadfmods/zshrc
    test -f "$xadfconfig/zshrc" && source "$xadfconfig/zshrc" || source "$xadfmods/zshrc"
fi

