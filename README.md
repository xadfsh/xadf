# XADF: a dotfiles management framework

## Table of Contents

1. [Dependencies](#dependencies)
2. [Installation](#installation)
3. [Usage](#usage)
4. [Customization](#customization)
5. [Appendix](#appendix)
   1. Repository branch structure
   2. Directory structure

## Dependencies

`xadf` is designed to be usable with the least amount of dependencies.
Therefore, the script incorporates only tools readily available to most systems,
while also tries to be minimal and simple.
Below is the list of dependencies `xadf` might require in a system:

- `bash` or `zsh` (might work on other shells that are at least POSIX-like)
- GNU coreutils
- `sed`: GNU sed
- `git`: it is an absolute necessity as the whole working of xadf is based on git
- `rsync`: current ad-hoc solution to sync contents from a repo to `$HOME`

## Obtaining the executables

Installation of xadf is usually done locally by first obtaining the main script itself, place it somewhere in your `$PATH` (or make one), make the script executable, and then you can use the script itself to bootstrap its own installation.

```
# Make directory, if it isn't present already
mkdir -p ~/.local/bin

# Download the executable into your local bin directory
wget -O ~/.local/bin/xadf https://gitlab.com/xadfsh/xadf/-/raw/master/.local/bin/xadf

# Make it executable
chmod +x ~/.local/bin/xadf

# Export path to local bin if it isn't set up already
PATH=~/.local/bin:$PATH

# Install xadf minimally
xadf --minimal-install
```

Alternatively you can just use our download script:

```
curl -o- https://gitlab.com/xadfsh/xadf/-/raw/master/install.sh | bash
```

## Installation

After `xadf` is included in your environment, and adding its location to your path, you can start to utilize the tool.
Note that at this point `xadf` is not yet installed, as you merely obtain the script and necessary files.
To fully utilize the script you must configure it.

`xadf` depends on a number of environment variables to be present and sourced from your `bashrc` or `zshrc`.
However you don't have to manually do it, as `xadf` can handle that for you with its configuration files.
The first thing you want to do is to install `xadf` in a manner that would be the most usable.

The following section assumes that you have included `xadf` executable in your path.

`xadf` supports three modes of installation:

### Install and configure

In this mode, `xadf` will install the executables, configuration files, and module files.
It would also attempt to inject a line in your `bashrc` or `zshrc` to source its configuration file.

```
xadf -i [--seat DIR] [--xadf-seat DIR] [--clone-source/-s URL] [--branch/-b BRANCH]
```

To change default git directory path to store your dotfiles repository, specify `--seat DIR`, where DIR is the path of your choice.
The default path is `~/.dotfiles`.

To change default git directory path to store the xadf repository, specify `--xadf-seat DIR`, where DIR is the path of your choice.
The default path is `~/.xadf`.

To specify the git branch to pull from a git repository, use `--branch/-b BRANCH` where BRANCH is the branch of your choice.
If `--clone-source/-s URL` is specified, BRANCH is the branch on repository URL.

It is recommended to first install and configure `xadf` with:

```
xadf -i [--xadf-seat DIR] [--seat DIR]
```

It would configure `xadf` by generating `xadfrc`, injecting a directive to source `xadfrc` on `bashrc` or `zshrc`, and generate the default `recipe.txt` file.

### Minimal install

In this mode `xadf` will only attempt to obtain the executable script and establish a basic `xadf` directory structure.

```
xadf --minimal-install [--xadf-seat DIR] [--branch BRANCH]
```

Note that `--xadf-seat DIR` is optional.
It would select a custom location DIR to place the git directory.
If it is not specified, then the default location is at `~/.xadf`.

The directory DIR can safely be removed afterwards, or you can use it to update `xadf` at later date with:

```
xadf -c DIR pull
```

The `--branch BRANCH` argument is also optional, and is normally not required.
It would specify which branch to pull from (the target branch must exist in [`xadfsh/xadf`](https://gitlab.com/xadfsh/xadf/)).
There are currently three branches:

- `base`: only files required for a minimal functioning of `xadf` is present.
  This is the default branch used by `xadf --minimal-install`.
- `main`: same as in `base` but also include extra files and modules.
- `master`: contains all files from `base` and `main`, along with some documentations.
  This branch is not meant to be used in a home directory, as it contains `README.md`, `LICENSE`, documentations, and an install script.

### Initialize a bare git repository

After `xadf` is installed to my home directory, I can configure it anyway I see fit.
To demonstrate the prowess of `xadf` implementation, here's how I can rapidly set up a new empty bare git repository:

```bash
# initialize, configure, load, and manage
xadf --init-bare --seat ~/.dotfiles
xadf --custom-install --seat ~/.dotfiles
. ~/.bashrc
xadf config status.showUntrackedFiles no
```

### Configure to use an existing dotfiles repository

It is possible that an existing dotfiles repository meant to be managed by bare git and alias method is already present.
One might already have it ready in their git repository, and they want to clone it to their home directory.
With `xadf`, it is possible to clone them and configure `xadfrc` accordingly to manage them, all with just a few lines:

```bash
# Clone and configure from a custom url, load, and manage
xadf -i -s git@gitlab.com:some_gitlab_user/mydotfiles.git --seat ~/.dotfiles
. ~/.bashrc
xadf status -sb
```

Another possibility is that one already have their own bare git repository at home directory.
Therefore they can configure their `xadfrc` to let `xadf` manage it.
Assuming they store their dotfiles bare git repository at `~/.dotfiles`, they can configure it in as easily as:

```bash
xadf --custom-install --seat ~/.dotfiles
. ~/.bashrc
```

## Usage

After an `xadf` installation is configured, and a dotfiles repository is already set up, one can use `xadf` as they would git, with their git work directory is set at `$HOME`.

### Managing dotfiles

For most cases, one can just use `xadf` as a git alias specifically to manage their dotfiles.
As they create new configuration files in their home directory, they might want to version control the configuration file.
Then they can add the file to their dotfiles repository in as simple as:

```bash
xadf status
xadf add .gitconfig
xadf commit -m 'Add gitconfig'
xadf push
```

### Updating and managing xadf core

`xadf` installation is also managed by `xadf` as a bare git repository.
Managing it is just as simple as appending `--core` argument before git-like commands.
For example to update your `xadf` installation, it can be done with:

```bash
xadf --core pull
```

To check the status of a `xadf` installation in case one have modified some of `xadf` core files, and they want to see how those files differ from the current `HEAD`, one can use the following:

```bash
xadf --core status
xadf --core diff
```

## Customization

`xadfrc` is not just a configuration file that enables `xadf` to manage one's dotfiles.
It is also capable of managing custom modules and configurations.

### custom modules

### alternative bashrc or zshrc

### drop-in directories

## Appendix

